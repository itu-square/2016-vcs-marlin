# Variation Control System #

Online appendix for the Variation Control System paper. 

### What is this repository for? ###

* Evaluation data
* Prototype (get and put function)
* Repositories used to conduct the experiment
* Updates executed by the variation control system

### Folders

Each folder has some documents related to our experiment. 

* metrics-data; Contains an Excel Spreadsheet with all the the changes, and extensive information (meta-data and metrics) about each of them. 
* repositories; The two repositories, Marlin and busybox(for cross validation of patch classification) used in the experiment as a zip file.
* vcs-updates; The 1 step and n-step updates realized by the variation control system. Each folder has the name of the sha. The file change has the original name as in the commit. Then there are files for projection, edit, update, original update from the original repository, and the original patch. All of these are abbreviated. 
* prototype; contains two jars for creating a view and updating a source code. More information in that directory
### Contact ###

* Stefan Stanciulescu, scas at itu dot dk